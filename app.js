const Agenda = require('agenda');

const dbURL = 'mongodb://127.0.0.1:27017/AgendaMedium';

const agenda = new Agenda({
    db: {address: dbURL, collection: 'Agenda'},
    processEvery: '20 seconds',
    useUnifiedTopology: true
});

agenda.define('log hello medium', async job => {
    const { name } = job.attrs;

    console.log(`Hello ${name} 👋`);

    /**
     * Replace the dummy log and write your code here
     */
});

agenda.define('happy new year', async job => {
    const year = new Date().getFullYear();
    console.log(`Happy new year ${year} 🎉`);

    /**
     * Replace the dummy log and write your code here
     */
});

(async function() {
    await agenda.start(); // Start Agenda instance

    await agenda.every('0 0 1 1 *', 'happy new year'); // Run the dummy job every year.

    await agenda.schedule('in 10 minutes', 'log hello medium', {name: 'Medium'}); // Run the dummy job in 10 minutes and passing data.
})();